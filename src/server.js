import https from 'https';
import fs from 'fs';
import nodeConstants from 'constants';

import app from './app';
import logger from './logger';

(async () => {
  const securedPort = process.env.SERVER_PORT_SECURED;
  const httpPort = process.env.SERVER_PORT;

  if (process.env.NODE_ENV === 'development') {
    app.listen(httpPort, () => logger.info(`development server running on ${httpPort}`));
  } else {
    try {
      // ssl config
      const httpsKey = fs.readFileSync('./certs/key.pem');
      const httpsCert = fs.readFileSync('./certs/cert.pem');
      const caInfinity = null;
      const credentials = {
        key: httpsKey,
        cert: httpsCert,
        ca: caInfinity,
        // eslint-disable-next-line no-bitwise
        secureOptions: nodeConstants.SSL_OP_NO_TLSv1 | nodeConstants.SSL_OP_NO_TLSv1_1,
        ciphers: [
          'ECDHE-RSA-AES256-SHA384',
          'DHE-RSA-AES256-SHA384',
          'ECDHE-RSA-AES256-SHA256',
          'DHE-RSA-AES256-SHA256',
          'ECDHE-RSA-AES128-SHA256',
          'DHE-RSA-AES128-SHA256',
          'HIGH',
          '!aNULL',
          '!eNULL',
          '!EXPORT',
          '!DES',
          '!RC4',
          '!MD5',
          '!PSK',
          '!SRP',
          '!CAMELLIA',
        ].join(':'),
        honorCipherOrder: true,
      };

      // https server
      const httpsServer = https.createServer(credentials, app);
      httpsServer.listen(securedPort, () => {
        logger.info(`https server listen to port ${securedPort}`, { method: 'server' });
      });
    } catch (err) {
      logger.info(`process exited : ${err}`, { method: 'Server init' });
      process.exit(1);
    }
  }
})();
