// external modules
import mongoose from 'mongoose';

// project modules
import logger from '../logger/winston';

// set up mongodb connection
/* Old example
const isProduction = process.env.NODE_ENV === 'prod';
const dbName = isProduction ? process.env.DB_NAME_PROD : process.env.DB_NAME_DEV;
const mongoDbURL = `${process.env.DB_URL}/${dbName}`;
*/

// TODO - real uri & name from env
const dbUri = 'mongodb://mongo:27017';
const dbName = 'template';

mongoose.connect(`${dbUri}${dbName}`, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: false,
});

// Get the default connection
const db = mongoose.connection;

// Bind connection to error event (to get notification of connection errors)
db.on('error', logger.error.bind(logger, 'MongoDB connection error:'));

export default db;
