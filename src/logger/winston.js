// external modules
import winston from 'winston';
import path from 'path';

import moment from 'moment';
import 'winston-daily-rotate-file';

const timeStampFormat = () => moment().format('DD-MM-YYYY HH:mm:ss').trim();

const fileTransport = new (winston.transports.DailyRotateFile)({
  level: 'info',
  filename: '%DATE%.log',
  datePattern: 'YYYY-MM-DD',
  zippedArchive: true,
  maxSize: '10m',
  maxFiles: '14d',
  dirname: path.join(path.resolve(__dirname, '../../'), 'logs'),
  format: winston.format.combine(
    winston.format.timestamp({ format: timeStampFormat }),
    winston.format.printf((info) => {
      if (!('method' in info)) {
        return `${info.timestamp} [${info.level}]: ${info.message}`;
      }
      return `${info.timestamp} [${info.level}] [${info.method}]: ${info.message}`;
    }),
  ),
});

fileTransport.on('rotate', (oldFilename, newFilename) => {
  // eslint-disable-next-line no-console
  console.info(`${new Date().toLocaleString()} [Logger]: log file ${oldFilename} rotated to ${newFilename}`);
});

const consoleTransport = new (winston.transports.Console)({
  level: 'debug',
  timestamp: timeStampFormat,
  format: winston.format.combine(
    winston.format.timestamp({ format: timeStampFormat }),
    winston.format.colorize(),
    winston.format.printf((info) => {
      if (!('method' in info)) {
        return `${info.timestamp} [${info.level}]: ${info.message}`;
      }
      return `${info.timestamp} [${info.level}] [${info.method}]: ${info.message}`;
    }),
  ),
});

// add transports according to environment
const allTransports = [];

switch (process.env.NODE_ENV) {
  case 'production':
  {
    allTransports.push(fileTransport);
    break;
  }
  case 'development':
  {
    allTransports.push(consoleTransport);
    break;
  }
  default:
  {
    // push console as default
    allTransports.push(consoleTransport);
  }
}

const logger = winston.createLogger({
  exceptionHandlers: allTransports,
  transports: allTransports,
});

export default logger;
