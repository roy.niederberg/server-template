export const extractNewKeycloakUser = async (request) => {
  const { firstName, lastName, email } = request.body;

  return { firstName, lastName, email };
};

export const convertKeycloakUserToIAMUser = (keycloakUser) => {
  const {
    firstName, lastName, email, id,
  } = keycloakUser;

  return {
    firstName, lastName, email, id,
  };
};
