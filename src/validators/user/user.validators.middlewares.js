import schemas from './user.validators.schemas';
import common from '../common';

const { validateSchema } = common;

const putUser = async (req, res, next) => validateSchema(schemas.putUser, 'body', req, next);
const getUser = async (req, res, next) => validateSchema(schemas.putUser, 'body', req, next);
const postUser = async (req, res, next) => validateSchema(schemas.putUser, 'body', req, next);

export default {
  postUser,
  getUser,
  putUser,
};
