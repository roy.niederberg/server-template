import schemas from './role.validators.schemas';
import common from '../common';

const { validateSchema } = common;

const idParam = async (req, res, next) => validateSchema(schemas.idParam, 'params', req, next);

const authHeader = async (req, res, next) => validateSchema(schemas.authHeader, 'headers', req, next);

const postRole = async (req, res, next) => validateSchema(schemas.postRole, 'body', req, next);

const putRole = async (req, res, next) => validateSchema(schemas.putRole, 'body', req, next);

export default {
  postRole,
  putRole,
  idParam,
  authHeader,
};
