import schemas from './group.validators.schemas';
import common from '../common';

const { validateSchema } = common;

const postGroup = async (req, res, next) => validateSchema(schemas.postGroup, 'body', req, next);
const postSubGroup = async (req, res, next) => validateSchema(schemas.postSubGroup, 'body', req, next);

const putGroup = async (req, res, next) => validateSchema(schemas.putGroup, 'body', req, next);

export default {
  postGroup,
  postSubGroup,
  putGroup,
};
