import Joi from 'joi';
import common from '../common';

// requests attributes definitions
const name = Joi.string().trim().regex(common.regex.groupName)
  .message(`${common.errMessages.groupName}`)
  .lowercase({ force: true });

// requests attributes definitions
const groupId = Joi.string().trim();

const postGroup = Joi.object().keys({
  name: name.required(),
});
const postSubGroup = Joi.object().keys({
  name,
  id: groupId,
}).xor('name', 'id');

const putGroup = Joi.object({
  name: name.required(),
});

export default {
  postSubGroup,
  postGroup,
  putGroup,
};
