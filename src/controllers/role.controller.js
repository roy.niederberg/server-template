import { convertKeycloakRoleToIamRole } from '../helpers/role';
import { generateMembershipRoleName, extractGroupIdFromMembershipRole } from '../helpers/membership';
import KeycloakAdapter from '../services/KeycloakAdapter/keycloakServer';

const createRole = async (req, res, next) => {
  try {
    const { name } = req.body;
    const kc = new KeycloakAdapter(req.headers.authorization);
    await kc.Roles.create({ name });
    res.status(201).send();
  } catch (err) {
    next(err);
  }
};

const getAllRoles = async (req, res, next) => {
  try {
    const kc = new KeycloakAdapter(req.headers.authorization);
    const roles = await kc.Roles.getAll();
    res.send(roles.map(convertKeycloakRoleToIamRole));
  } catch (err) {
    next(err);
  }
};

const updateRoleById = async (req, res, next) => {
  try {
    const { roleid } = req.params;
    const { name } = req.body;
    const kc = new KeycloakAdapter(req.headers.authorization);
    const compositeRoles = await kc.Roles.getCompositesById(roleid);

    // composite roles are membership roles  - `${groupId}::{roleName}, should be updated`
    await Promise.all(
      compositeRoles.map(((compositeRole) => kc.ClientRoles.updateById(compositeRole.id, {
        name: generateMembershipRoleName(
          extractGroupIdFromMembershipRole(compositeRole.name),
          name,
        ),
      }))),
    );
    await kc.Roles.updateById(roleid, { name });
    res.send();
  } catch (err) {
    next(err);
  }
};

const deleteRoleById = async (req, res, next) => {
  try {
    const { roleid } = req.params;
    const kc = new KeycloakAdapter(req.headers.authorization);

    // ClientRoles are composites of Roles
    const compositeRoles = await kc.Roles.getCompositesById(roleid);
    await Promise.all(
      compositeRoles.map((compositeRole) => kc.Roles.deleteById(compositeRole.id)),
    );

    await kc.Roles.deleteById(roleid);
    res.send();
  } catch (err) {
    next(err);
  }
};

const getRoleById = async (req, res, next) => {
  try {
    const { roleid } = req.params;
    const kc = new KeycloakAdapter(req.headers.authorization);
    const role = await kc.Roles.getById(roleid);
    res.send(convertKeycloakRoleToIamRole(role));
  } catch (err) {
    next(err);
  }
};

export default {
  createRole,
  getAllRoles,
  getRoleById,
  updateRoleById,
  deleteRoleById,
};
