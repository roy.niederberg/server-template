import KeycloakAdapter from '../services/KeycloakAdapter/keycloakServer';
import { isMembershipRoleInGroup, generateMembershipRoleName, extractRoleFromMembershipRole } from '../helpers/membership';
import { ErrorHandler } from '../helpers/error';
import { convertKeycloakUserToIAMUser } from '../helpers/user';
import { convertKeycloakRoleToIamRole } from '../helpers/role';

const getGroupMemberRolesImp = async (kc, groupid, userid) => {
  const clientRoles = await kc.ClientRoles.getByUserId(userid);
  const membershipRolesNames = clientRoles
    .filter((role) => isMembershipRoleInGroup(groupid, role.name))
    .map((role) => extractRoleFromMembershipRole(role.name));
  const [...membershipRoles] = await Promise.all(membershipRolesNames
    .map((name) => kc.Roles.getByName(name)));
  return membershipRoles.map(convertKeycloakRoleToIamRole);
};

const getGroupMemberRoles = async (req, res, next) => {
  try {
    const { groupid, userid } = req.params;
    const kc = new KeycloakAdapter(req.headers.authorization);
    const membershipRoles = await getGroupMemberRolesImp(kc, groupid, userid);

    res.send(membershipRoles);
  } catch (err) {
    next(err);
  }
};

const addGroupMemberRole = async (req, res, next) => {
  try {
    const { groupid, roleid, userid } = req.params;
    const kc = new KeycloakAdapter(req.headers.authorization);

    const groupsPromise = kc.Users.getGroupsById(userid);
    const rolePromise = kc.Roles.getById(roleid);
    const [groups, role] = await Promise.all([groupsPromise, rolePromise]);

    const group = groups.find((candidateGroup) => candidateGroup.id === groupid);
    if (!group) {
      throw new ErrorHandler(404, 'user not in group');
    }
    const membershipRoleName = generateMembershipRoleName(group.id, role.name);
    let membershipRole;
    try {
      membershipRole = await kc.ClientRoles.getByName(membershipRoleName);
    } catch (err) {
      if (err.response.status === 404) {
        await kc.ClientRoles.create({ name: membershipRoleName });
        membershipRole = await kc.ClientRoles.getByName(membershipRoleName);
        await kc.Roles.addCompositesById(roleid, membershipRole);
      } else {
        throw err;
      }
    }

    await kc.ClientRoles.addByUserId(userid, membershipRole);
    res.send();
  } catch (err) {
    next(err);
  }
};

const removeGroupMemberRole = async (req, res, next) => {
  try {
    const { groupid, roleid, userid } = req.params;
    const kc = new KeycloakAdapter(req.headers.authorization);

    const groupPromise = kc.Groups.getById(groupid);
    const rolePromise = kc.Roles.getById(roleid);
    const [group, role] = await Promise.all([groupPromise, rolePromise]);
    const membershipRole = await kc.ClientRoles
      .getByName(generateMembershipRoleName(group.id, role.name));
    await kc.ClientRoles.deleteByUserId(userid, membershipRole);
    res.send();
  } catch (err) {
    next(err);
  }
};
const addGroupMember = async (req, res, next) => {
  try {
    const { groupid, userid } = req.params;
    const kc = new KeycloakAdapter(req.headers.authorization);
    await kc.Groups.addMember(groupid, userid);
    res.send();
  } catch (err) {
    next(err);
  }
};

const removeGroupMember = async (req, res, next) => {
  try {
    const { groupid, userid } = req.params;
    const kc = new KeycloakAdapter(req.headers.authorization);
    const removePromise = kc.Groups.removeMember(groupid, userid);
    const groupPromise = kc.Groups.getById(groupid);
    const [group] = await Promise.all([
      groupPromise,
      removePromise,
    ]);

    let rolesToRemove = [];
    try {
      const clientRoles = await kc.ClientRoles.getByUserId(userid);
      rolesToRemove = clientRoles
        .filter((role) => isMembershipRoleInGroup(group.id, role.name));
    } catch (err) {
      // no client Roles
    }
    await Promise.all(rolesToRemove
      .map((role) => kc.ClientRoles.deleteByUserId(userid, role)));
    res.send();
  } catch (err) {
    next(err);
  }
};

const getAllGroupMembers = async (req, res, next) => {
  try {
    const { groupid } = req.params;
    const kc = new KeycloakAdapter(req.headers.authorization);
    let members = await kc.Groups.getMembers(groupid);
    members = members.map(convertKeycloakUserToIAMUser);
    if (req.query.roles === true) {
      const rolesPromises = [];
      for (let index = 0; index < members.length; index += 1) {
        rolesPromises.push(getGroupMemberRolesImp(kc, groupid, members[index].id));
      }
      const membersRoles = await Promise.all(rolesPromises);
      members = members.map((member, index) => ({ ...member, roles: membersRoles[index] }));
    }
    res.send(members);
  } catch (err) {
    next(err);
  }
};
export default {
  removeGroupMemberRole,
  addGroupMemberRole,
  getGroupMemberRoles,
  addGroupMember,
  removeGroupMember,
  getAllGroupMembers,
};
