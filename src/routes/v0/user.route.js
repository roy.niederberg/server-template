import express from 'express';
import controllers from '../../controllers';
import validators from '../../validators';

const router = express.Router();

router
  .route('/')
  .get(controllers.user.getAllUsers)
  .post(validators.user.postUser, controllers.user.createUser);

router
  .route('/:userid')
  .get(validators.user.getUser, controllers.user.getUser)
  .delete(controllers.user.deleteUser)
  .put(validators.user.putUser, controllers.user.updateUser);

export default router;
