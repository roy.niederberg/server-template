module.exports = {
  moduleFileExtensions: [
    'mjs',
    'js',
    'ts',
    'tsx',
    'json',
    'node',
  ],
  transform: {
    '^.+\\.[t|j]sx?$': 'babel-jest',
  },
  setupFiles: [
    '<rootDir>/src/tests/setup.js',
    'dotenv/config',
  ],
  moduleNameMapper: {
    '^anotherRoot/(.*)$': '<rootDir>/lib/anotherRoot/$1',
  },
  roots: [
    'src/tests',
  ],
  testMatch: [
    '**/*.test.js', '**/?(*.)(spec|test).js?(x)',
    '**/test/**/*.js', '**/?(*.)(spec|test).mjs',
  ],
  transformIgnorePatterns: [],
  testEnvironment: 'node',
};
