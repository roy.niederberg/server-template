FROM node:latest

WORKDIR /usr/src/app

COPY package*.json ./

RUN apt update && apt install -y
RUN npm install 

COPY .babelrc ./
COPY .env ./
COPY jest.config.js ./
COPY certs ./certs
COPY src ./src

EXPOSE 4040 8080

CMD ["npm","run", "start:dev"]
